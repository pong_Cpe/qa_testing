<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQaMacrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qa_macros', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('qa_base_id');
            $table->string('status_micro')->nullable();
            $table->string('status_usage')->nullable();
            $table->integer('freezing_year')->nullable();
            $table->integer('freezing_month')->nullable();
            $table->integer('freezing_week')->nullable();
            $table->integer('freezing_day')->nullable();
            $table->string('tpc_10dilution')->nullable();
            $table->string('cg_10dilution')->nullable();
            $table->string('cg_10dilution_petrifilm')->nullable();
            $table->string('e_coli')->nullable();
            $table->string('yeast_status')->nullable();
            $table->string('mold_Status')->nullable();
            $table->integer('tpc_positive_count')->nullable();
            $table->integer('cg_positive_count')->nullable();
            $table->integer('sample_count')->nullable();
            $table->integer('count_cg0')->nullable();
            $table->integer('count_cg5')->nullable();
            $table->integer('count_cg10_20')->nullable();
            $table->integer('count_cg25_45')->nullable();
            $table->integer('count_cg50_95')->nullable();
            $table->integer('count_cg100up')->nullable();
            $table->integer('count_kernel03low')->nullable();
            $table->integer('count_kernel030_039')->nullable();
            $table->integer('count_kernel040_049')->nullable();
            $table->integer('count_kernel050_059')->nullable();
            $table->integer('count_kernel060_069')->nullable();
            $table->integer('count_kernel070_079')->nullable();
            $table->integer('count_kernel080_089')->nullable();
            $table->integer('count_kernel090_099')->nullable();
            $table->integer('count_kernel100up')->nullable();
            $table->integer('count_pod100low')->nullable();
            $table->integer('count_pod100_109')->nullable();
            $table->integer('count_pod110_119')->nullable();
            $table->integer('count_pod120_129')->nullable();
            $table->integer('count_pod130_139')->nullable();
            $table->integer('count_pod140_149')->nullable();
            $table->integer('count_pod150_159')->nullable();
            $table->integer('count_pod160_169')->nullable();
            $table->integer('count_pod170_179')->nullable();
            $table->integer('count_pod180_189')->nullable();
            $table->integer('count_pod190_199')->nullable();
            $table->integer('count_pod200up')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qa_macros');
    }
}
