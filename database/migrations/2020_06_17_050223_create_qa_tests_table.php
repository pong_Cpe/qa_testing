<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQaTestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qa_tests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('qa_base_id');
            $table->string('crop')->nullable();
            $table->integer('tpc')->nullable();
            $table->integer('coliform_colonies')->nullable();
            $table->integer('coliform_confirm')->nullable();
            $table->integer('coliform_petrifilm')->nullable();
            $table->integer('e_coli_g')->nullable();
            $table->integer('yeast_g')->nullable();
            $table->integer('mold_g')->nullable();
            $table->float('perc_salt_pod')->nullable();
            $table->float('perc_salt_kernel')->nullable();
            $table->float('perc_salt')->nullable();
            $table->float('ph')->nullable();
            $table->float('ph_kernel')->nullable();
            $table->float('tss')->nullable();
            $table->float('tss_kernel')->nullable();
            $table->float('perc_ta')->nullable();
            $table->float('hardness')->nullable();
            $table->float('hardness_calculate')->nullable();
            $table->integer('num_worm_pod')->nullable();
            $table->float('weight_worm_pod')->nullable();
            $table->float('perc_worm_pod')->nullable();
            $table->float('total_weight')->nullable();
            $table->float('viscosity')->nullable();
            $table->float('spec_mc')->nullable();
            $table->string('sensory_test')->nullable();
            $table->string('enzyme_solid')->nullable();
            $table->string('enzyme_liquid')->nullable();
            $table->string('enzyme_peroxidase')->nullable();
            $table->string('pallet')->nullable();
            $table->string('line')->nullable();
            $table->string('hr_loc')->nullable();
            $table->string('note1')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qa_tests');
    }
}
