<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQaBasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qa_bases', function (Blueprint $table) {
            $table->increments('id');
            $table->string('run_no')->nullable();
            $table->date('store_date')->nullable();
            $table->string('store_shift')->nullable();
            $table->string('code_seq')->nullable();
            $table->date('test_date')->nullable();
            $table->date('sending_date')->nullable();
            $table->string('sampling_no')->nullable();
            $table->string('product_type')->nullable();
            $table->string('product_code')->nullable();
            $table->string('code_supplier')->nullable();
            $table->string('farmer_village')->nullable();
            $table->string('production_code')->nullable();
            $table->time('random_time')->nullable();
            $table->string('box_no')->nullable();
            $table->date('freeze_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qa_bases');
    }
}
